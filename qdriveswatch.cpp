#include "qdriveswatch.h"

#include <QDir>
#include <QDebug>

QDrivesWatch::QDrivesWatch(QObject *parent) :
    QObject(parent)
{
    filename = "smup.smu";
}

void QDrivesWatch::setFilename(const QString &_filename)
{
    filename = _filename;
}

QString QDrivesWatch::getFilename()
{
    return filename;
}

void QDrivesWatch::init()
{
    // clear old list
    knownFiles.clear();

    // check all drives in Windows
    QFileInfoList drivesList;
#ifdef Q_OS_WIN
    drivesList = QDir::drives();
    for(int num=0;num<drivesList.size();num++) {
        QFileInfo info = drivesList[num];
        if(QDir().exists(info.absoluteFilePath()+filename)) {
            // drive with smup file found
            // add to list
            knownFiles.append(info.absoluteFilePath()+filename);
            qDebug() << "DrivesWatch init found" << info.absoluteFilePath()+filename;
        }
    }
#endif
#ifdef Q_OS_LINUX
    QDir directory;
    directory.setPath(QString("/media/%1").arg(getenv("USER")));
    qDebug() << "Drives watch init " << directory.path();
    directory.setFilter(QDir::AllEntries | QDir::Hidden | QDir::NoDotAndDotDot);
    drivesList = directory.entryInfoList();
    for(int num=0;num<drivesList.size();num++) {
        QFileInfo info = drivesList[num];
        if(QDir().exists(info.absoluteFilePath()+"/"+filename)) {
            // drive with smup file found
            // add to list
            knownFiles.append(info.absoluteFilePath()+"/"+filename);
            qDebug() << "DrivesWatch init found" << info.absoluteFilePath()+"/"+filename;
        }
    }
#endif

}

void  QDrivesWatch::watch()
{
    // clear old list
    QStringList newKnownFiles;

    // check all drives in Windows
    QFileInfoList drivesList;
#ifdef Q_OS_WIN
    drivesList = QDir::drives();
    for(int num=0;num<drivesList.size();num++) {
        QFileInfo info = drivesList[num];
        if(QDir().exists(info.absoluteFilePath()+filename)) {
            // drive with smup file found
            // add to list
            newKnownFiles.append(info.absoluteFilePath()+filename);
            // check if this file is new
            if(!knownFiles.contains(info.absoluteFilePath()+filename)) {
                qDebug() << "DrivesWatch found" << info.absoluteFilePath()+filename;
                emit newFile(info.absoluteFilePath()+filename,info.absoluteFilePath());
            }
        }
    }
#endif
#ifdef Q_OS_LINUX
    QDir directory;
    directory.setPath(QString("/media/%1").arg(getenv("USER")));
    qDebug() << "Drives watch update " << directory.path();
    directory.setFilter(QDir::AllEntries | QDir::Hidden | QDir::NoDotAndDotDot);
    drivesList = directory.entryInfoList();
    for(int num=0;num<drivesList.size();num++) {
        QFileInfo info = drivesList[num];
        if(QDir().exists(info.absoluteFilePath()+"/"+filename)) {
            // drive with smup file found
            // add to list
            newKnownFiles.append(info.absoluteFilePath()+"/"+filename);
            // check if this file is new
            if(!knownFiles.contains(info.absoluteFilePath()+"/"+filename)) {
                qDebug() << "DrivesWatch found" << info.absoluteFilePath()+"/"+filename;
                emit newFile(info.absoluteFilePath()+"/"+filename,info.absoluteFilePath());
            }
        }
    }
#endif

    // check if file was removed
    for(int num=0;num<knownFiles.size();num++)
    {
        if(!newKnownFiles.contains(knownFiles[num]))
        {
            qDebug() << "DrivesWatch removed" << knownFiles[num];
            emit removedFile(knownFiles[num]);
        }
    }

    // save new list
    knownFiles = newKnownFiles;
}
