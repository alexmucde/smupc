#include <QDir>
#include <QDebug>

#include "smupbackup.h"

#include "smupsource.h"

SmupSource::SmupSource(QObject *parent) :
    QThread(parent)
{
    clear();

    withMD5 = false;
}

int  SmupSource::size()
{
    return smupBackups.size();
}

SmupBackup*  SmupSource::operator[](int pos)
{
    return smupBackups[pos];
}

void SmupSource::addBackup(SmupBackup *backup)
{
    smupBackups.append(backup);
}

void SmupSource::deleteBackup(SmupBackup *backup)
{
    smupBackups.removeOne(backup);
    delete backup;
}

void SmupSource::clear()
{
    while (!smupBackups.isEmpty())
        delete smupBackups.takeFirst();

    dir.clear();
    drive.clear();
    host.clear();

    reset();
}

void SmupSource::reset()
{
    state = SmupSource::init;

    counterFiles = 0;
    counterDirectories = 0;
    counterSize = 0;

    files.clear();
}

void SmupSource::scan()
{
    counterFiles = 0;
    counterDirectories = 0;
    counterSize = 0;

    files.clear();

    scanDir("");

}

void SmupSource::scanDir(QString dirname)
{
    QDir directory;
    if(dirname.isEmpty())
    {
        if(QDir::isAbsolutePath(dir))
            directory.setPath(dir);
        else
            directory.setPath(drive+dir);
        qDebug() << directory.path();
    }
    else
    {
        dirname += "/";
        if(QDir::isAbsolutePath(dir))
            directory.setPath(dir+"/"+dirname);
        else
            directory.setPath(drive+dir+"/"+dirname);
    }
    directory.setFilter(QDir::AllEntries | QDir::Hidden | QDir::NoDotAndDotDot);
    QFileInfoList list = directory.entryInfoList();

    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list[i];
        if(fileInfo.isFile())
        {
            counterFiles++;
            counterSize+=fileInfo.size();
            QByteArray md5;
            if(withMD5)
                md5 = SmupFile::calculateMD5(fileInfo.absoluteFilePath());
            if(dirname.isEmpty())
                files[fileInfo.fileName()] = SmupFile(fileInfo.size(),SmupFile::source,md5);
            else
                files[dirname+fileInfo.fileName()] = SmupFile(fileInfo.size(),SmupFile::source,md5);
        }
        else if(fileInfo.isDir())
        {
            counterDirectories++;
            if(dirname.isEmpty())
                scanDir(fileInfo.fileName());
            else
                scanDir(dirname+fileInfo.fileName());

        }
    }
}

void SmupSource::run()
{
    scan();

    state = SmupSource::finish;
    qDebug() << "Finished source scan thread " << getDir();
}

void SmupSource::startScan()
{
    qDebug() << "Start source scan thread " << getDir();
    state = SmupSource::scanning;

    start();
}
