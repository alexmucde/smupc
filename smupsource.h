#ifndef SMUPSOURCE_H
#define SMUPSOURCE_H

#include <QObject>
#include <QDir>
#include <QThread>
#include <QMap>
#include <QTreeWidgetItem>
#include <QMap>

#include "smupfile.h"

class SmupBackup;

class SmupSource : public QThread
{   
    Q_OBJECT

    friend class SmupBackup;

public:
    enum State {init,scanning,finish};

    explicit SmupSource(QObject *parent = 0);
    virtual ~SmupSource() { disconnect();clear(); }

    int size();
    SmupBackup* operator[](int pos);
    void addBackup(SmupBackup *backup);
    void deleteBackup(SmupBackup *backup);

    void clear();
    void reset();

    void setDir(const QString& _dir) { dir = _dir; }
    QString getDir() { return dir; }

    void setDrive(const QString& _drive) { drive = _drive; }
    QString getDrive() { return drive; }

    void setHost(const QString& _host) { host = _host; }
    QString getHost() { return host; }

    void setItem(QTreeWidgetItem* _item) { item = _item; }
    QTreeWidgetItem* getItem() { return item; }

    unsigned int getFiles() { return counterFiles; }
    quint64 getFilesSize() { return counterSize; }

    State getState() { return state; }

    QMap<QString,SmupFile> getFilesMap() { return files; }

    void startScan();

    void setWithMD5(bool _withMD5) { withMD5 = _withMD5; }

private:

    QString dir;
    QString drive;
    QString host;

    QList<SmupBackup*> smupBackups;

    QTreeWidgetItem *item;

    QMap<QString,SmupFile> files;

    unsigned int counterFiles;
    unsigned int counterDirectories;
    quint64 counterSize;

    State state;

    bool withMD5;

    void scanDir(QString dirname);
    void scan();
    void run();

signals:
    
public slots:

};

#endif // SMUPSOURCE_H
