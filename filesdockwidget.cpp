#include "filesdockwidget.h"
#include "ui_filesdockwidget.h"

FilesDockWidget::FilesDockWidget(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::FilesDockWidget)
{
    ui->setupUi(this);

    ui->treeWidget->setColumnWidth(0,150); // Filename
    ui->treeWidget->setColumnWidth(1,50);  // Operation

}

FilesDockWidget::~FilesDockWidget()
{
    delete ui;
}


void FilesDockWidget::clear()
{
    ui->treeWidget->clear();
}

void FilesDockWidget::updateTreeFiles(const QMap<QString,SmupFile> &map,int type)
{
    ui->treeWidget->clear();

    QMapIterator<QString,SmupFile> i(map);
    while (i.hasNext()) {
        i.next();

        QString text = i.key();
        SmupFile file = i.value();

        if(type == 0 ||
           (type == 1 && (file.state==SmupFile::unequal || file.state==SmupFile::source || file.state==SmupFile::backup)) ||
           (type == 2 && (file.state==SmupFile::unequal || file.state==SmupFile::source)) ||
           (type == 3 && (file.state==SmupFile::backup)))
        {
            QTreeWidgetItem *item = new QTreeWidgetItem();
            item->setData(0,Qt::DisplayRole,text);
            switch(file.state)
            {
                case SmupFile::unknown:
                    item->setData(1,Qt::DisplayRole,"unknown");
                    break;
                case SmupFile::equal:
                    item->setData(1,Qt::DisplayRole,"Nothing");
                    break;
                case SmupFile::unequal:
                    item->setData(1,Qt::DisplayRole,"Copy");
                    break;
                case SmupFile::source:
                    item->setData(1,Qt::DisplayRole,"New");
                    break;
                case SmupFile::backup:
                    item->setData(1,Qt::DisplayRole,"Delete");
                    break;
            }
            ui->treeWidget->addTopLevelItem(item);
        }
    }
}

