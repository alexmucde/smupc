#ifndef SMUPBACKUPDIALOG_H
#define SMUPBACKUPDIALOG_H

#include <QDialog>

#include "smupbackup.h"

namespace Ui {
class SmupBackupDialog;
}

class SmupBackupDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SmupBackupDialog(QWidget *parent = 0);
    ~SmupBackupDialog();

    void load(SmupBackup &backup);
    void save(SmupBackup &backup);

private slots:
    void on_pushButtonDirectory_clicked();

private:
    Ui::SmupBackupDialog *ui;
};

#endif // SMUPBACKUPDIALOG_H
