echo off
echo *************************
echo * Configuration         *
echo *************************

echo Setting up environment for Qt usage...

set QTDIR=C:\Qt\5.6\mingw49_32
set MINGW_DIR=C:\Qt\Tools\mingw492_32

set PATH=%QTDIR%\bin;%MINGW_DIR%\bin;%PATH%
set QTSDK=%QTDIR%

echo *************************
echo * QTDIR = %QTDIR%
echo * MINGW_DIR = %MINGW_DIR%
echo *************************

echo Setting up environment for DLT Viewer SDK...

set SDK_DIR=c:\smupc
set PWD=%~dp0
set SOURCE_DIR=%PWD%
set BUILD_DIR=%PWD%build\release

echo *************************
echo * Build smupc           *
echo *************************

mkdir build
cd build
qmake ../smupc.pro
mingw32-make.exe release

echo *************************
echo * Create smupc          *
echo *************************

echo Create directories
mkdir %SDK_DIR%
mkdir %SDK_DIR%\platforms

echo Copy files
copy %QTDIR%\bin\icuin54.dll %SDK_DIR%
copy %QTDIR%\bin\icuuc54.dll %SDK_DIR%
copy %QTDIR%\bin\icudt54.dll %SDK_DIR%
copy %QTDIR%\bin\libwinpthread-1.dll %SDK_DIR%
copy %QTDIR%\bin\libgcc_s_dw2-1.dll %SDK_DIR%
copy "%QTDIR%\bin\libstdc++-6.dll" %SDK_DIR%
copy %QTDIR%\bin\Qt5Core.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5Gui.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5Network.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5Sql.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5Svg.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5Widgets.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5PrintSupport.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5Xml.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5OpenGL.dll %SDK_DIR%
copy %QTDIR%\bin\Qt5WinExtras.dll %SDK_DIR%
copy %QTDIR%\plugins\platforms\qwindows.dll %SDK_DIR%\platforms

copy %BUILD_DIR%\smupc.exe %SDK_DIR%

echo *************************
echo * Finish                *
echo *************************
echo smupc installed in:  %SDK_DIR%
set /p name= Continue
