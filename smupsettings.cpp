#include <QDir>

#include "smupsettings.h"

SmupSettings::SmupSettings()
{
    settings = new QSettings(QDir::currentPath()+"/smupc.ini",QSettings::IniFormat);
}

SmupSettings::~SmupSettings()
{
    delete settings;
}

bool SmupSettings::getEnableAutomaticFunctions()
{
    return settings->value("enableAutomaticFunctions",false).toBool();
}

bool SmupSettings::getAutoProjectLoading()
{
    return settings->value("autoProjectLoading",false).toBool();
}

bool SmupSettings::getAutoProjectLoadingStartup()
{
    return settings->value("autoProjectLoadingStartup",false).toBool();
}

bool SmupSettings::getAutoProjectClosing()
{
    return settings->value("autoProjectClosing",false).toBool();
}

bool SmupSettings::getUseMD5()
{
    return settings->value("autoUseMD5",false).toBool();
}
