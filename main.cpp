#include <QApplication>
#include "smupmainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SmupMainWindow w;
    w.show();
    
    return a.exec();
}
