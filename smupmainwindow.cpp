#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QHostInfo>

#include "smupmainwindow.h"
#include "ui_smupmainwindow.h"
#include "ui_smupform.h"
#include "projectselectiondialog.h"
#include "infodialog.h"
#include "settingsdialog.h"

SmupMainWindow::SmupMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SmupMainWindow),
    smupForm(this),
    drivesWatchTimer(this),
    progressBar(this)
{
    ui->setupUi(this);

    smupForm.setFilesDockWidget(&filesWidget);
    smupForm.init(&smupProject);

    setCentralWidget(&smupForm);
    addDockWidget(Qt::RightDockWidgetArea,&filesWidget);
    addDockWidget(Qt::RightDockWidgetArea,&jobsWidget);

    ui->statusBar->addWidget(&progressBar,25);
    progressBar.show();
#ifdef Q_OS_WIN
    taskbarButton = new QWinTaskbarButton(this);
    taskbarButton->setWindow(windowHandle());
    taskbarProgress = taskbarButton->progress();
    taskbarProgress->setVisible(true);
    taskbarProgress->show();
#endif

    ui->statusBar->addWidget(&currentJob,75);
    currentJob.show();

    connect(&smupJobBroker,SIGNAL(currentJob(QString)),this,SLOT(currentJobShow(QString)));
    connect(&smupJobBroker,SIGNAL(finished()),this,SLOT(jobsFinished()));

    currentJob.setText("Open project");

#ifdef Q_OS_WIN
    drivesWatch.setFilename("smup.smu");
#endif
#ifdef Q_OS_LINUX
    drivesWatch.setFilename("smup_ubuntu.smu");
#endif
    drivesWatch.init();
    connect(&drivesWatch, SIGNAL(newFile(QString,QString)), this, SLOT(newFile(QString,QString)));
    connect(&drivesWatch, SIGNAL(removedFile(QString)), this, SLOT(removedFile(QString)));
    connect(&drivesWatchTimer, SIGNAL(timeout()), this, SLOT(checkDrivesWatch()));
    drivesWatchTimer.start(1000);

    updateTitle();

    state = open;
    statemachineFinish();

    if(settings.getAutoProjectLoadingStartup())
        on_actionSearch_Project_triggered(true);
}

SmupMainWindow::~SmupMainWindow()
{
    setCentralWidget(0);

    delete ui;
}

void SmupMainWindow::showEvent(QShowEvent *e)
{
#ifdef Q_OS_WIN
    taskbarButton->setWindow(windowHandle());
#endif

    QMainWindow::showEvent(e);
}

void SmupMainWindow::on_actionOpen_triggered()
{
   QString fileName = QFileDialog::getOpenFileName(this,
         tr("Open Project"), "", tr("Smup Files (*.smu)"));

   if(fileName.isEmpty())
       return;

   QFileInfo info(fileName);
   loadProject(fileName,info.absolutePath());
}

void SmupMainWindow::on_actionSave_2_triggered()
{
    if(smupProject.getFilename().isEmpty())
    {
        on_actionSave_As_triggered();
        return;
    }

    smupProject.save(smupProject.getFilename());

    updateTitle();
}

void SmupMainWindow::on_actionSave_As_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
          tr("Save Project"), smupProject.getFilename(), tr("Smup Files (*.smu)"));

    if(!fileName.isEmpty())
    {
        smupProject.save(fileName);
        updateTitle();
    }

}

void SmupMainWindow::on_actionSearch_Project_triggered(bool startup)
{
    QStringList knownFiles = drivesWatch.getKnownFiles();

    if(knownFiles.size()>0)
    {
        ProjectSelectionDialog dlg(this);
        dlg.setFiles(knownFiles);
        if(dlg.exec())
        {
            QFileInfo info(dlg.getSelectedFile());
            loadProject(info.filePath(),info.absolutePath());
        }
    }
    else
    {
        if(!startup)
            QMessageBox::information(this,"Project search","No project found!");
    }
}

void SmupMainWindow::newFile(QString filename, QString drive)
{
    if(settings.getAutoProjectLoading())
        loadProject(filename,drive);
}

void SmupMainWindow::removedFile(QString filename)
{
    if(settings.getAutoProjectClosing())
    {
        qDebug() << "removedFile" << filename;
        if(smupProject.getFilename()==filename)
        {
            qDebug() << "getFilename" << smupProject.getFilename();
            smupProject.clear();
            smupForm.init(&smupProject);
            smupJobBroker.clear();            
            filesWidget.clear();
            jobsWidget.clear();
        }
    }
}

void SmupMainWindow::loadProject(QString filename,QString drive)
{
    smupProject.clear();
    smupProject.load(filename,drive);

    updateProject();
    updateTitle();

    jobsWidget.clear();
    smupJobBroker.clear();
    smupForm.update();

    state = open;
    statemachineFinish();
}

void SmupMainWindow::updateProject()
{
    for(int num1=0;num1<smupProject.size();num1++)
    {
        SmupSource *source = smupProject[num1];
        disconnect(source,SIGNAL(finished()),0,0);
        connect(source,SIGNAL(finished()),&smupForm,SLOT(update()));
        connect(source,SIGNAL(finished()),this,SLOT(update()));
        for(int num2=0;num2<source->size();num2++)
        {
            SmupBackup *backup = (*source)[num2];
            backup->setSource(source);
            disconnect(backup,SIGNAL(finished()),0,0);
            connect(backup,SIGNAL(finished()),&smupForm,SLOT(update()));
            connect(backup,SIGNAL(finished()),this,SLOT(update()));
        }
    }

    smupForm.init(&smupProject);
}

void SmupMainWindow::on_actionClear_triggered()
{
    smupProject.reset();
    smupForm.update();
    smupJobBroker.clear();
    jobsWidget.clear();
    progressBar.reset();
#ifdef Q_OS_WIN
    taskbarProgress->reset();
#endif
    filesWidget.clear();

    state = open;
    statemachineFinish();
}

void SmupMainWindow::on_actionSources_triggered()
{
    if(smupProject.size()==0)
    {
        QMessageBox::warning(this,"Scan Sources","No sources configured!");
        return;
    }

    state = scanSources;
    statemachineStart();
}

void SmupMainWindow::on_actionBackups_triggered()
{
    int backups = 0;
    for(int num1=0;num1<smupProject.size();num1++)
    {
        SmupSource *source = smupProject[num1];
        for(int num2=0;num2<source->size();num2++)
        {
            //SmupBackup *backup = (*source)[num2];
            backups++;
        }
    }

    if(backups==0)
    {
        QMessageBox::warning(this,"Scan Backups","No backups configured!");
        return;
    }

    state = scanBackups;
    statemachineStart();
}

void SmupMainWindow::on_actionAdd_triggered()
{
     for(int num1=0;num1<smupProject.size();num1++) {
         SmupSource *source = smupProject[num1];
          for(int num2=0;num2<source->size();num2++) {
             SmupBackup *backup = (*source)[num2];
             addJobs(backup);
         }
     }
}

void SmupMainWindow::addJobs(SmupBackup *backup)
{
    int type = 1;
    if(backup->getJob()=="sync")
    {
        type = 1;
    }
    else if(backup->getJob()=="copy")
    {
        type = 2;
    }
    else if(backup->getJob()=="backup")
    {
        qDebug() << "Backup not supported yet";
    }
    QString sourceDir,backupDir;
    if(QDir::isAbsolutePath(backup->getSource()->getDir()))
        sourceDir = backup->getSource()->getDir();
    else
        sourceDir = backup->getSource()->getDrive() + backup->getSource()->getDir();
    if(QDir::isAbsolutePath(backup->getDir()))
        backupDir = backup->getDir();
    else
        backupDir = backup->getDrive() + backup->getDir();
    qDebug() << "Add Jobs for" << sourceDir << backupDir;
    smupJobBroker.addJobs(backup->getFilesMap(),type,sourceDir,backupDir);
    if(smupJobBroker.size()>0)
    {
        progressBar.setMaximum(smupJobBroker.size());
#ifdef Q_OS_WIN
        taskbarProgress->setMaximum(smupJobBroker.size());
#endif
        progressBar.setValue(0);
#ifdef Q_OS_WIN
        taskbarProgress->setValue(0);
#endif
        currentJob.setText(QString("%1 files to operate - Start execute!").arg(smupJobBroker.size()));
    }
    else
    {
        progressBar.reset();
#ifdef Q_OS_WIN
        taskbarProgress->reset();
#endif
    }

}

void SmupMainWindow::currentJobShow(QString operation)
{

    int secondsSinceStart = smupJobBroker.getStartTime().secsTo(QTime::currentTime());
    quint64 sizeFilesAll = smupJobBroker.getSizeFiles();
    quint64 sizeFilesCopied = smupJobBroker.getSizeFilesCopied();
    if(sizeFilesCopied==0) sizeFilesCopied = 1;
    int secondsEstimated = (int)((quint64)sizeFilesAll * secondsSinceStart / (quint64)sizeFilesCopied) ;
    int secondsRemaining = secondsEstimated - secondsSinceStart;

    progressBar.setValue(progressBar.maximum()-smupJobBroker.size());
#ifdef Q_OS_WIN
    taskbarProgress->setValue(taskbarProgress->maximum()-smupJobBroker.size());
#endif
    currentJob.setText(QString("%1 jobs (Estimated %2:%3)").arg(smupJobBroker.getJobs()-smupJobBroker.getJobsFinished())
                       .arg(secondsRemaining/60).arg(secondsRemaining%60,2,10,QLatin1Char('0')));

    jobsWidget.update(smupJobBroker);
}

void SmupMainWindow::jobsFinished()
{
    progressBar.reset();    
#ifdef Q_OS_WIN
    taskbarProgress->reset();
#endif
    statemachineFinish();
}

void SmupMainWindow::on_actionStart_triggered()
{
    state = startJobs;
    statemachineStart();
}

void SmupMainWindow::on_actionStop_triggered()
{
    smupJobBroker.clear();
    state = finish;
    statemachineStop();
}

void SmupMainWindow::update()
{
    bool sourcesScan = false;
    bool backupsScan = false;
    bool sourcesScanFinish = true;
    bool backupsScanFinish = true;

    for(int num1=0;num1<smupProject.size();num1++) {
        SmupSource *source = smupProject[num1];
        if(source->getState()==SmupSource::scanning)
            sourcesScan = true;
        if(source->getState()!=SmupSource::finish)
            sourcesScanFinish = false;
        for(int num2=0;num2<source->size();num2++) {
            SmupBackup *backup = (*source)[num2];
            if(backup->getState()==SmupBackup::scanning)
                backupsScan = true;
            if(backup->getState()!=SmupBackup::finish)
                backupsScanFinish = false;
        }
    }

    if(!sourcesScan && !backupsScan)
    {
        statemachineFinish();
    }
}

void SmupMainWindow::on_actionSettings_triggered()
{
    SettingsDialog dlg(this);
    dlg.loadSettings(smupProject);
    if(dlg.exec())
    {
        dlg.saveSettings(smupProject);
        smupProject.setChanged();
        updateTitle();
    }

}

void SmupMainWindow::checkDrivesWatch()
{
    // called every second
    drivesWatch.watch();
}

void SmupMainWindow::on_actionInfo_triggered()
{
    InfoDialog dlg(this);
    dlg.exec();
}

void SmupMainWindow::statemachineStart()
{
    switch(state)
    {
        case init:
            qDebug() << "Start init";
            break;
        case open:
            qDebug() << "Start open";
            break;
        case scanSources:
            qDebug() << "Start scanSources";
#ifdef Q_OS_WIN
            taskbarProgress->setMaximum(0);
#endif
#ifdef Q_OS_WIN
            taskbarButton->setOverlayIcon(QIcon(":/icons/icons/Sync.png"));
#endif
            smupProject.scanSources(settings.getUseMD5());
            smupForm.update();
            filesWidget.clear();
            break;
        case scanBackups:
            qDebug() << "Start scanBackups";
#ifdef Q_OS_WIN
            taskbarProgress->setMaximum(0);
#endif
#ifdef Q_OS_WIN
            taskbarButton->setOverlayIcon(QIcon(":/icons/icons/Sync.png"));
#endif
            smupProject.scanBackups(settings.getUseMD5());
            smupForm.update();
            filesWidget.clear();
            break;
        case startJobs:
            qDebug() << "Start startJobs";
#ifdef Q_OS_WIN
            taskbarButton->setOverlayIcon(QIcon(":/icons/icons/Go.png"));
#endif
            smupJobBroker.start();
            break;
        case scanBackups2:
            qDebug() << "Start scanBackups2";
            smupProject.scanBackups();
            smupForm.update();
            filesWidget.clear();
            break;
        case finish:
            qDebug() << "Start finish";
            break;
    }

    currentJob.setText("Please wait!");

    // enable buttons
    ui->actionOpen->setEnabled(false);
    ui->actionSearch_Project->setEnabled(false);
    ui->actionClear->setEnabled(false);
    ui->actionSources->setEnabled(false);
    ui->actionBackups->setEnabled(false);
    ui->actionAdd->setEnabled(false);
    ui->actionStart->setEnabled(false);
    ui->actionStop->setEnabled(true);

}

void SmupMainWindow::statemachineStop()
{
    switch(state)
    {
        case init:
            break;
        case open:
            break;
        case scanSources:
            state = open;
            currentJob.setText("Next scan sources!");
            break;
        case scanBackups:
            state = scanSources;
            currentJob.setText("Next start jobs!");
            break;
        case startJobs:
            state = scanBackups;
            currentJob.setText("Finished!");
            break;
        case scanBackups2:
            state = startJobs;
            currentJob.setText("Finished!");
            break;
        case finish:
#ifdef Q_OS_WIN
            taskbarProgress->setMaximum(100);
#endif
#ifdef Q_OS_WIN
            taskbarButton->setOverlayIcon(QIcon());
#endif

            break;
    }

    // enable buttons
    updateButtons();
}

void SmupMainWindow::statemachineFinish()
{
    switch(state)
    {
        case init:
            qDebug() << "Finish init";
            currentJob.setText("Open a project!");
            break;
        case open:
            qDebug() << "Finish open project";
            currentJob.setText("Next scan sources!");
            if(settings.getEnableAutomaticFunctions() && (smupProject.getAuto()=="scan" || smupProject.getAuto()=="job"))
            {
                state = scanSources;
                statemachineStart();
                return;
            }
            break;
        case scanSources:
            qDebug() << "Finish scanSources";
#ifdef Q_OS_WIN
            taskbarProgress->setMaximum(100);
#endif
#ifdef Q_OS_WIN
            taskbarButton->setOverlayIcon(QIcon());
#endif
            currentJob.setText("Next scan backups!");
            if(settings.getEnableAutomaticFunctions() && (smupProject.getAuto()=="scan" || smupProject.getAuto()=="job"))
            {
                state = scanBackups;
                statemachineStart();
                return;
            }
            break;
        case scanBackups:
            qDebug() << "Finish scanBackups";
#ifdef Q_OS_WIN
            taskbarProgress->setMaximum(100);
#endif
#ifdef Q_OS_WIN
            taskbarButton->setOverlayIcon(QIcon());
#endif
            currentJob.setText("Next add jobs start jobs!");
            if(settings.getEnableAutomaticFunctions() && smupProject.getAuto()=="job")
            {
                for(int num1=0;num1<smupProject.size();num1++) {
                    SmupSource *source = smupProject[num1];
                     for(int num2=0;num2<source->size();num2++) {
                        SmupBackup *backup = (*source)[num2];
                        addJobs(backup);
                    }
                }
                state = startJobs;
                statemachineStart();
                return;
            }
            break;
        case startJobs:
            qDebug() << "Finish startJobs";
#ifdef Q_OS_WIN
            taskbarButton->setOverlayIcon(QIcon());
#endif
            currentJob.setText("Finished!");
            if(settings.getEnableAutomaticFunctions() && smupProject.getAuto()=="job")
            {
                state = scanBackups2;
                statemachineStart();
                return;
            }
            break;
        case scanBackups2:
            qDebug() << "Finish scanBackups2";
            currentJob.setText("Finished!");
            state = finish;
            break;
        case finish:
            qDebug() << "Finish finish";
            currentJob.setText("Finished!");
            state = finish;
            break;
    }

    // enable buttons
    updateButtons();

}

void SmupMainWindow::updateButtons()
{
    ui->actionOpen->setEnabled(true);
    ui->actionSearch_Project->setEnabled(true);
    ui->actionClear->setEnabled(state>=scanSources);
    ui->actionSources->setEnabled(state>=open);
    ui->actionBackups->setEnabled(state>=scanSources);
    ui->actionAdd->setEnabled(state>=scanBackups);
    ui->actionStart->setEnabled(state>=scanBackups);
    ui->actionStop->setEnabled(false);
}

void SmupMainWindow::on_actionAdd_Source_triggered()
{
    smupForm.addSource();

    updateProject();
    updateTitle();
}

void SmupMainWindow::on_actionAdd_Backup_triggered()
{
    smupForm.addBackup();

    updateProject();
    updateTitle();
}

void SmupMainWindow::on_actionDelete_triggered()
{
    smupForm.deleteItem();
    updateTitle();
}

void SmupMainWindow::on_actionEdit_triggered()
{
    smupForm.editItem();
    updateTitle();
}

void SmupMainWindow::updateTitle()
{
    if(smupProject.getFilename().isEmpty())
    {
        setWindowTitle(QString("SMUP Client (%1)").arg(QHostInfo::localHostName()));
    }
    else
    {
        if(smupProject.getChanged())
        {
            setWindowTitle(smupProject.getFilename() + QString("* - SMUP Client (%1)").arg(QHostInfo::localHostName()));
        }
        else
        {
            setWindowTitle(smupProject.getFilename() + QString(" - SMUP Client (%1)").arg(QHostInfo::localHostName()));
        }
    }
}

