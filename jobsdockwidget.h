#ifndef JOBSDOCKWIDGET_H
#define JOBSDOCKWIDGET_H

#include <QDockWidget>

#include "smupjobbroker.h"

namespace Ui {
class JobsDockWidget;
}

class JobsDockWidget : public QDockWidget
{
    Q_OBJECT
    
public:
    explicit JobsDockWidget(QWidget *parent = 0);
    ~JobsDockWidget();
    
    void clear();
    void update(SmupJobBroker &broker);

private:
    Ui::JobsDockWidget *ui;
};

#endif // JOBSDOCKWIDGET_H
