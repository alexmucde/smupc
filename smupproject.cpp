#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDebug>
#include <QMessageBox>
#include <QHostInfo>

#include "smupproject.h"

SmupProject::SmupProject(QObject *parent) :
    QObject(parent)
{
}

SmupProject::~SmupProject()
{
    // clear when deleteing object
    clear();
}

void SmupProject::clear()
{
    while (!smupSources.isEmpty())
        delete smupSources.takeFirst();

    projectFilename.clear();
    automation.clear();

    changed = false;

    drive.clear();
}

void SmupProject::reset()
{
    for(int num1=0;num1<smupSources.size();num1++)
    {
        SmupSource *source = smupSources[num1];
        source->reset();
        for(int num2=0;num2<source->size();num2++)
        {
            SmupBackup *backup = (*source)[num2];
            backup->reset();
        }
    }
}

int  SmupProject::size()
{
    return smupSources.size();
}

SmupSource*  SmupProject::operator[](int pos)
{
    return smupSources[pos];
}

bool SmupProject::load(const QString &filename,QString _drive)
{
    QFile file(filename);
    bool open = file.open(QIODevice::ReadOnly | QIODevice::Text);
    if (!open)
    {
        qDebug() << "QSmupProject: Open of file " << filename << " failed!";
        QMessageBox::warning(0,"Project open","Open of project " + filename + " failed");
        return false;
    }

    QXmlStreamReader xml(&file);
    SmupSource *source = 0;
    SmupBackup *backup = 0;

    drive = _drive;
    if(!drive.endsWith("/\\"))
        drive += '/';

    while (!xml.atEnd() && !xml.hasError()) {
        xml.readNext();
        if (xml.isStartElement()) {
            if(xml.name().toString() == "auto") {
                automation = xml.readElementText();
            }
            if(xml.name().toString() == "source") {
                // create new source
                source = new SmupSource();
            }
            if(xml.name().toString() == "backup") {
                // create new backup
                backup = new SmupBackup();
            }
            if(xml.name().toString() == "path") {
                QString text = xml.readElementText();
                if(backup) {
                    qDebug() << "Backup" << backup->getDir();
                    backup->setDir(text);
                }
                else if(source) {
                    qDebug() << "Source" << source->getDir();
                    source->setDir(text);
                }
            }
            if(xml.name().toString() == "host") {
                QString text = xml.readElementText();
                if(source) {
                    source->setHost(text);
                }
            }
            if(xml.name().toString() == "job") {
                QString text = xml.readElementText();
                if(backup) {
                    backup->setJob(text);
                }
            }
        }
        else if (xml.isEndElement())
        {
            if(xml.name().toString() == "source") {
                if(source) {
                    // add source
                    smupSources.append(source);
                }
                source = 0;
            }
            if(xml.name().toString() == "backup") {
                if(backup) {
                    if(source)
                        // add backup
                        source->addBackup(backup);
                    else
                        /* error: wrong XML format */
                        delete backup;
                }
                backup = 0;
            }
        }
    }
    if(xml.hasError()) {
        // XML Parser error
        qDebug() << "QSmupProject: " << "XML Error: " <<  xml.errorString();

        // clear project
        clear();
    }

    file.close();

    projectFilename = filename;
    changed = false;

    return true;
}

bool SmupProject::save(const QString &filename)
{    
    QFile file(filename);

    bool open = file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
    if (!open)
    {
        qDebug() << "QSmupProject: Save of file " << filename << " failed!";
        QMessageBox::warning(0,"Project save","Save of project " + filename + " failed");
        return false;
    }

    QXmlStreamWriter xml(&file);

    xml.setAutoFormatting(true);
    xml.writeStartDocument();

    xml.writeStartElement("smupproject");

    xml.writeTextElement("auto",getAuto());

    // write all sources
    for(int num = 0;num<smupSources.size();num++) {
        SmupSource *source = smupSources[num];
        xml.writeStartElement("source");

        xml.writeTextElement("host",source->getHost());
        xml.writeTextElement("path",source->getDir());

        // write all backups
        for(int num2=0;num2<source->size();num2++)
        {
            SmupBackup *backup = (*source)[num2];
            xml.writeStartElement("backup");

            xml.writeTextElement("job",backup->getJob());
            xml.writeTextElement("path",backup->getDir());

            xml.writeEndElement(); // backup
       }

        xml.writeEndElement(); // source
    }

    xml.writeEndElement(); // smupproject

    xml.writeEndDocument();

    file.close();

    projectFilename = filename;
    changed = false;

    return true;
}

void SmupProject::scanSources(bool withMd5)
{
    for(int num=0;num<smupSources.size();num++)
    {
        SmupSource *source = smupSources[num];
        for(int num2=0;num2<source->size();num2++)
        {
            SmupBackup *backup = (*source)[num2];
            backup->reset();
        }
        source->setWithMD5(withMd5);
        source->setDrive(drive);
        if(source->getHost().isEmpty() || (QHostInfo::localHostName()==source->getHost()))
            source->startScan();
    }
}

void SmupProject::scanBackups(bool withMd5)
{
    for(int num1=0;num1<smupSources.size();num1++)
    {
        SmupSource *source = smupSources[num1];
        if(source->getHost().isEmpty() || (QHostInfo::localHostName()==source->getHost()))
        {
            for(int num2=0;num2<source->size();num2++)
            {
                SmupBackup *backup = (*source)[num2];
                backup->setWithMD5(withMd5);
                backup->setDrive(drive);
                qDebug() << "drive" << drive;
                backup->startScan();
            }
        }
    }
}


