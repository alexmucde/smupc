#include "smupjobbroker.h"

#include <QFile>
#include <QFileInfo>
#include <QDir>

SmupJobBroker::SmupJobBroker(QObject *parent) :
    QThread(parent)
{
    clear();
}

void SmupJobBroker::addJob(SmupJob &job)
{
    if(job.operation == SmupJob::copy)
        sizeFiles += job.size();
    jobs.enqueue(job);
    jobsAll++;
}

int SmupJobBroker::size()
{
    return jobs.size();
}

quint64 SmupJobBroker::getSizeFiles()
{
    return sizeFiles;
}

quint64 SmupJobBroker::getSizeFilesCopied()
{
    return sizeFilesCopied;
}

QTime SmupJobBroker::getStartTime()
{
    return startTime;
}

void SmupJobBroker::clear()
{
    jobs.clear();
    sizeFiles = 0;
    sizeFilesCopied = 0;
    jobsAll = 0;
    jobsFinished = 0;
}

void SmupJobBroker::run()
{
    startTime = QTime::currentTime();

    while (!jobs.isEmpty())
    {
        SmupJob job = jobs.dequeue();

        switch(job.operation)
        {
            case SmupJob::none:
                emit currentJob(QString("none"));
                break;
            case SmupJob::copy:
            {
                emit currentJob(QString("copy %1").arg(job.file));
                QFileInfo info(job.target+"/"+job.file);
                QDir dir("/");
                dir.mkpath(info.absolutePath());
                if(QFile::exists(job.target+"/"+job.file))
                    QFile::remove(job.target+"/"+job.file); // delete if already exist
                QFile::copy(job.source+"/"+job.file,job.target+"/"+job.file);
                sizeFilesCopied += job.size();
                break;
            }
            case SmupJob::erase:
            {
                emit currentJob(QString("delete %1").arg(job.file));
                QFile::remove(job.target+"/"+job.file);
                break;
            }
        }
        jobsFinished++;
    }
}

int SmupJobBroker::addJobs(const QMap<QString,SmupFile> &map,int type,const QString &sourceDir,const QString &backupDir)
{
    int jobsAdded = 0;
    QMapIterator<QString,SmupFile> i(map);

    while (i.hasNext()) {
        i.next();

        QString text = i.key();
        SmupFile file = i.value();

        if(type == 0 ||
           (type == 1 && (file.state==SmupFile::unequal || file.state==SmupFile::source || file.state==SmupFile::backup)) ||
           (type == 2 && (file.state==SmupFile::unequal || file.state==SmupFile::source)) ||
           (type == 3 && (file.state==SmupFile::backup)))
        {
            switch(file.state)
            {
                case SmupFile::unknown:
                    break;
                case SmupFile::equal:
                    break;
                case SmupFile::unequal:
                case SmupFile::source:
                    {
                    /* add copy job */
                    SmupJob job;
                    job.operation = SmupJob::copy;
                    job.file = text;
                    job.source = sourceDir;
                    job.target = backupDir;
                    job.sizeFile = file.size;
                    addJob(job);
                    jobsAdded++;
                    break;
                    }
                case SmupFile::backup:
                    {
                    /* add erase job */
                    SmupJob job;
                    job.operation = SmupJob::erase;
                    job.file = text;
                    job.source = "";
                    job.target = backupDir;
                    addJob(job);
                    jobsAdded++;
                    break;
                    }
            }
        }
    }

    return jobsAdded;
}
