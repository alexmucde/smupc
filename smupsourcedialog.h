#ifndef SMUPSOURCEDIALOG_H
#define SMUPSOURCEDIALOG_H

#include <QDialog>

#include "smupsource.h"

namespace Ui {
class SmupSourceDialog;
}

class SmupSourceDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SmupSourceDialog(QWidget *parent = 0);
    ~SmupSourceDialog();

    void load(SmupSource &source);
    void save(SmupSource &source);
    
private slots:
    void on_pushButtonDirectory_clicked();

    void on_pushButtonClear_clicked();

    void on_pushButtonHost_clicked();

private:
    Ui::SmupSourceDialog *ui;
};

#endif // SMUPSOURCEDIALOG_H
