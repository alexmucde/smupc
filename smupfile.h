#ifndef SMUPFILE_H
#define SMUPFILE_H

class SmupFile
{
public:
    enum State {unknown,equal,unequal,source,backup};
    SmupFile() {
        size = 0;
        state = SmupFile::unknown;
    }
    SmupFile(quint64 _size,State _state,QByteArray _md5 = QByteArray()) {
        size = _size;
        state = _state;
        md5 = _md5;
    }
    SmupFile& operator = (const SmupFile &file)
    {
        size = file.size;
        state = file.state;
        md5 = file.md5;

        return *this;
    }

    static QByteArray calculateMD5(const QString &filename);

    State state;
    quint64 size;
    QByteArray md5;
};

#endif // SMUPFILE_H
