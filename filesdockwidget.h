#ifndef FILESDOCKWIDGET_H
#define FILESDOCKWIDGET_H

#include <QDockWidget>

#include "smupfile.h"

namespace Ui {
class FilesDockWidget;
}

class FilesDockWidget : public QDockWidget
{
    Q_OBJECT
    
public:
    explicit FilesDockWidget(QWidget *parent = 0);
    ~FilesDockWidget();

    void updateTreeFiles(const QMap<QString,SmupFile> &map,int type);

    void clear();

private:
    Ui::FilesDockWidget *ui;
};

#endif // FILESDOCKWIDGET_H
