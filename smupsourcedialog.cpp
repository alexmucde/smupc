#include <QFileDialog>
#include <QHostInfo>

#include "smupsourcedialog.h"
#include "ui_smupsourcedialog.h"

#include "smupsource.h"

SmupSourceDialog::SmupSourceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SmupSourceDialog)
{
    ui->setupUi(this);
}

SmupSourceDialog::~SmupSourceDialog()
{
    delete ui;
}

void SmupSourceDialog::on_pushButtonDirectory_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Source Directory"),
                                                 "",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);

    if(!dir.isEmpty())
    {
        ui->lineEditDirectory->setText(dir);
    }
}

void SmupSourceDialog::on_pushButtonClear_clicked()
{
    ui->lineEditHost->clear();
}

void SmupSourceDialog::on_pushButtonHost_clicked()
{
    ui->lineEditHost->setText(QHostInfo::localHostName());
}

void SmupSourceDialog::load(SmupSource &source)
{
     ui->lineEditHost->setText(source.getHost());
     ui->lineEditDirectory->setText(source.getDir());
}

void SmupSourceDialog::save(SmupSource &source)
{
    source.setHost(ui->lineEditHost->text());
    source.setDir(ui->lineEditDirectory->text());
}
