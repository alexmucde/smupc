#include "projectselectiondialog.h"
#include "ui_projectselectiondialog.h"

ProjectSelectionDialog::ProjectSelectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProjectSelectionDialog)
{
    ui->setupUi(this);
}

ProjectSelectionDialog::~ProjectSelectionDialog()
{
    delete ui;
}

void ProjectSelectionDialog::setFiles(const QStringList &files)
{
    ui->listWidget->addItems(files);
    ui->listWidget->selectedItems();
    ui->listWidget->item(0)->setSelected(true);
}

QString ProjectSelectionDialog::getSelectedFile()
{
    return ui->listWidget->currentItem()->text();
}

void ProjectSelectionDialog::on_listWidget_itemDoubleClicked(QListWidgetItem */*item*/)
{
    done(QDialog::Accepted);
}
