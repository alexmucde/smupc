#ifndef SMUPFORM_H
#define SMUPFORM_H

#include <QWidget>

#include "smupproject.h"

#include "filesdockwidget.h"

namespace Ui {
class SmupForm;
}

class SmupForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit SmupForm(QWidget *parent = 0);
    ~SmupForm();
    
    void setFilesDockWidget(FilesDockWidget *widget) { filesDockWidget = widget; }

    void init(SmupProject *_project);

    void updateTreeWidget();

    void addSource();
    void addBackup();
    void editItem();
    void deleteItem();


    Ui::SmupForm *ui;

public slots:

    void update();

private slots:
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int);

    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);

private:
    SmupProject *project;

    FilesDockWidget *filesDockWidget;
};

#endif // SMUPFORM_H
