#ifndef SMUPMAINWINDOW_H
#define SMUPMAINWINDOW_H

#include <QMainWindow>
#include <QProgressBar>
#include <QLabel>
#include <QTimer>

#ifdef Q_OS_WIN
    #include <QWinTaskbarButton>
    #include <QWinTaskbarProgress>
#endif

#include "smupproject.h"
#include "smupform.h"
#include "filesdockwidget.h"
#include "jobsdockwidget.h"
#include "smupjobbroker.h"
#include "qdriveswatch.h"
#include "smupsettings.h"

namespace Ui {
class SmupMainWindow;
}

class SmupMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    enum State {init,open,scanSources,scanBackups,startJobs,scanBackups2,finish};

    explicit SmupMainWindow(QWidget *parent = 0);
    ~SmupMainWindow();
    
public slots:

    void update();
    void currentJobShow(QString);
    void jobsFinished();
    void newFile(QString filename, QString drive);
    void removedFile(QString filename);
    void checkDrivesWatch();

private slots:

    void on_actionOpen_triggered();

    void on_actionSources_triggered();

    void on_actionBackups_triggered();

    void on_actionStart_triggered();

    void on_actionStop_triggered();

    void on_actionClear_triggered();

    void on_actionAdd_triggered();

    void on_actionSettings_triggered();

    void on_actionInfo_triggered();

    void on_actionSearch_Project_triggered(bool startup = false);

    void on_actionSave_2_triggered();

    void on_actionSave_As_triggered();

    void on_actionAdd_Source_triggered();

    void on_actionDelete_triggered();

    void on_actionEdit_triggered();

    void on_actionAdd_Backup_triggered();

protected:
    virtual void showEvent(QShowEvent *e);

private:
    Ui::SmupMainWindow *ui;
    SmupForm smupForm;
    FilesDockWidget filesWidget;
    JobsDockWidget jobsWidget;

    SmupProject smupProject;
    SmupJobBroker smupJobBroker;

    QDrivesWatch drivesWatch;
    QTimer drivesWatchTimer;

    SmupSettings settings;

    QProgressBar progressBar;
    QLabel currentJob;

#ifdef Q_OS_WIN
    QWinTaskbarButton *taskbarButton;
    QWinTaskbarProgress *taskbarProgress;
#endif

    State state;

    void statemachineStart();
    void statemachineStop();
    void statemachineFinish();
    void updateButtons();

    void loadProject(QString filename,QString drive = QString());
    void updateProject();
    void updateTitle();

    void addJobs(SmupBackup *backup);


};

#endif // SMUPMAINWINDOW_H
