#include <QDir>
#include <QDebug>

#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    qDebug() << "Settings" << QDir::currentPath()+"/smupc.ini";

    settings = new QSettings(QDir::currentPath()+"/smupc.ini",QSettings::IniFormat);
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
    delete settings;
}

void SettingsDialog::loadSettings(SmupProject &project)
{
    // global
    ui->checkBoxEnableAutomaticFunctions->setChecked(settings->value("enableAutomaticFunctions",false).toBool());
    ui->checkBoxAutoProjectLoading->setChecked(settings->value("autoProjectLoading",false).toBool());
    ui->checkBoxAutoProjectLoadingStartup->setChecked(settings->value("autoProjectLoadingStartup",false).toBool());
    ui->checkBoxAutoProjectClosing->setChecked(settings->value("autoProjectClosing",false).toBool());
    ui->checkBoxUseMD5->setChecked(settings->value("autoUseMD5",false).toBool());

    // project
    int index = ui->comboBoxAutomation->findText(project.getAuto());
    if ( index != -1 ) { // -1 for not found
       ui->comboBoxAutomation->setCurrentIndex(index);
    }

}

void SettingsDialog::saveSettings(SmupProject &project)
{
    // global
    settings->setValue("enableAutomaticFunctions",ui->checkBoxEnableAutomaticFunctions->isChecked());
    settings->setValue("autoProjectLoading",ui->checkBoxAutoProjectLoading->isChecked());
    settings->setValue("autoProjectLoadingStartup",ui->checkBoxAutoProjectLoadingStartup->isChecked());
    settings->setValue("autoProjectClosing",ui->checkBoxAutoProjectClosing->isChecked());
    settings->setValue("autoUseMD5",ui->checkBoxUseMD5->isChecked());

    // project
    project.setAuto(ui->comboBoxAutomation->currentText());
}
