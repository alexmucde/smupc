#ifndef SMUPJOBBROKER_H
#define SMUPJOBBROKER_H

#include <QThread>
#include <QQueue>
#include <QMap>
#include <QTime>

#include "smupfile.h"

class SmupJob
{
public:
    enum Operation {none,copy,erase};
    SmupJob() {
         operation = SmupJob::none;
         sizeFile = 0;
    }
    SmupJob& operator = (const SmupJob &job)
    {
        operation = job.operation;
        file = job.file;
        source = job.source;
        target = job.target;
        sizeFile = job.sizeFile;

        return *this;
    }
    quint64 const size()
    {
        return sizeFile;
    }

    Operation operation;
    QString file;
    QString source;
    QString target;
    quint64 sizeFile;
};

class SmupJobBroker : public QThread
{
    Q_OBJECT
public:
    explicit SmupJobBroker(QObject *parent = 0);
    
    void addJob(SmupJob &job);
    int size();
    quint64 getSizeFiles();
    quint64 getSizeFilesCopied();
    QTime getStartTime();

    int getJobs() { return jobsAll; }
    int getJobsFinished() { return jobsFinished; }

    void clear();
    void run();

    int addJobs(const QMap<QString,SmupFile> &map,int type,const QString &sourceDir,const QString &backupDir);

private:

    QQueue<SmupJob> jobs;
    quint64 sizeFiles; // size of all files to be copied
    quint64 sizeFilesCopied; // size of all files already copied

    int jobsAll;
    int jobsFinished;

    QTime startTime;

signals:

    void currentJob(QString);

public slots:
    
};

#endif // SMUPJOBBROKER_H
