#include "jobsdockwidget.h"
#include "ui_jobsdockwidget.h"

JobsDockWidget::JobsDockWidget(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::JobsDockWidget)
{
    ui->setupUi(this);
}

JobsDockWidget::~JobsDockWidget()
{
    delete ui;
}

void JobsDockWidget::clear()
{
    ui->lineEdit_jobs->clear();
    ui->lineEdit_jobs_finished->clear();

    ui->lineEdit_copy_size->clear();
    ui->lineEdit_copy_size_done->clear();

    ui->lineEdit_start_time->clear();
    ui->lineEdit_duration->clear();
    ui->lineEdit_finish_time->clear();

    ui->lineEdit_speed->clear();
}

void JobsDockWidget::update(SmupJobBroker &broker)
{
    int secondsSinceStart = broker.getStartTime().secsTo(QTime::currentTime());
    quint64 sizeFilesAll = broker.getSizeFiles();
    quint64 sizeFilesCopied = broker.getSizeFilesCopied();
    if(sizeFilesCopied==0) sizeFilesCopied = 1;
    int secondsEstimated = (int)((quint64)sizeFilesAll * secondsSinceStart / (quint64)sizeFilesCopied) ;
    if(secondsEstimated==0) secondsEstimated = 1;
    int secondsRemaining = secondsEstimated - secondsSinceStart;

    ui->lineEdit_jobs->setText(QString("%1").arg(broker.getJobs()));
    ui->lineEdit_jobs_finished->setText(QString("%1").arg(broker.getJobsFinished()));

    quint64 size = sizeFilesAll;
    QString sizeString;
    if(size<1000)
        sizeString = QString("%1 Bytes").arg(size);
    else if(size<1000000)
        sizeString = QString("%1.%2 Bytes").arg(size/1000ul).arg(size%1000ul,3,10,QLatin1Char('0'));
    else if(size<1000000000)
        sizeString = QString("%1,%2 MBytes").arg(size/1000000ul).arg((size/1000ul)%1000ul,3,10,QLatin1Char('0'));
    else
        sizeString = QString("%1,%2 GBytes").arg(size/Q_UINT64_C(1000000000)).arg((size/1000000ul)%1000ul,3,10,QLatin1Char('0'));
    ui->lineEdit_copy_size->setText(QString("%1").arg(sizeString));
    size = sizeFilesCopied;
    if(size<1000)
        sizeString = QString("%1 Bytes").arg(size);
    else if(size<1000000)
        sizeString = QString("%1.%2 Bytes").arg(size/1000ul).arg(size%1000ul,3,10,QLatin1Char('0'));
    else if(size<1000000000)
        sizeString = QString("%1,%2 MBytes").arg(size/1000000ul).arg((size/1000ul)%1000ul,3,10,QLatin1Char('0'));
    else
        sizeString = QString("%1,%2 GBytes").arg(size/Q_UINT64_C(1000000000)).arg((size/1000000ul)%1000ul,3,10,QLatin1Char('0'));
    ui->lineEdit_copy_size_done->setText(QString("%1").arg(sizeString));

    ui->lineEdit_start_time->setText(QString("%1").arg(broker.getStartTime().toString("hh:mm:ss")));
    ui->lineEdit_duration->setText(QString("%1:%2").arg(secondsEstimated/60,2,10,QLatin1Char('0')).arg(secondsEstimated%60,2,10,QLatin1Char('0')));
    ui->lineEdit_finish_time->setText(QString("%1").arg(broker.getStartTime().addSecs(secondsEstimated).toString("hh:mm:ss")));

    size = sizeFilesCopied/secondsEstimated;
    if(size<1000)
        sizeString = QString("%1 Bytes/s").arg(size);
    else if(size<1000000)
        sizeString = QString("%1.%2 Bytes/s").arg(size/1000ul).arg(size%1000ul,3,10,QLatin1Char('0'));
    else if(size<1000000000)
        sizeString = QString("%1,%2 MBytes/s").arg(size/1000000ul).arg((size/1000ul)%1000ul,3,10,QLatin1Char('0'));
    else
        sizeString = QString("%1,%2 GBytes/s").arg(size/Q_UINT64_C(1000000000)).arg((size/1000000ul)%1000ul,3,10,QLatin1Char('0'));
    ui->lineEdit_speed->setText(QString("%1").arg(sizeString));
}
