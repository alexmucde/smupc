#ifndef PROJECTSELECTIONDIALOG_H
#define PROJECTSELECTIONDIALOG_H

#include <QDialog>
#include <QStringList>
#include <QListWidgetItem>

namespace Ui {
class ProjectSelectionDialog;
}

class ProjectSelectionDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ProjectSelectionDialog(QWidget *parent = 0);
    ~ProjectSelectionDialog();

    void setFiles(const QStringList &files);
    QString getSelectedFile();
    
private slots:
    void on_listWidget_itemDoubleClicked(QListWidgetItem *);

private:
    Ui::ProjectSelectionDialog *ui;
};

#endif // PROJECTSELECTIONDIALOG_H
