#include <QDebug>

#include "smupbackup.h"

SmupBackup::SmupBackup(QObject *parent) :
    QThread(parent)
{
    clear();

    withMD5 = false;

}

void SmupBackup::clear()
{

    dir.clear();
    drive.clear();

    reset();

}

void SmupBackup::reset()
{
    state = SmupBackup::init;

    counterFiles = 0;
    counterFilesNew = 0;
    counterFilesCopy = 0;
    counterFilesDelete = 0;
    counterDirectories = 0;
    counterSize = 0;

    files.clear();

}

void SmupBackup::scan()
{
    counterFiles = 0;
    counterFilesNew = 0;
    counterFilesCopy = 0;
    counterFilesDelete = 0;
    counterDirectories = 0;
    counterSize = 0;

    files.clear();

    // copy filelist from source
    if(source)
    {
        counterFilesNew = source->files.size();
        files = source->files;
    }

    scanDir("");

}

void SmupBackup::scanDir(QString dirname)
{
    QDir directory;
    if(dirname.isEmpty())
    {
        if(QDir::isAbsolutePath(dir))
            directory.setPath(dir);
        else
            directory.setPath(drive+dir);
        qDebug() << directory.path();
    }
    else
    {
        dirname += "/";
        if(QDir::isAbsolutePath(dir))
            directory.setPath(dir+"/"+dirname);
        else
            directory.setPath(drive+dir+"/"+dirname);
    }
    directory.setFilter(QDir::AllEntries | QDir::Hidden | QDir::NoDotAndDotDot);
    QFileInfoList list = directory.entryInfoList();

    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list[i];
        if(fileInfo.isFile())
        {
            counterFiles++;
            counterSize+=fileInfo.size();
            QMap<QString,SmupFile>::const_iterator i = files.find(dirname+fileInfo.fileName());
            SmupFile file;
            if(i != files.end()) {
                // file from backup found in source
                counterFilesNew--;
                file = i.value();
                if(withMD5)
                {
                    QByteArray md5;
                    md5 = SmupFile::calculateMD5(fileInfo.absoluteFilePath());
                    if(file.md5==md5)
                    {
                        // source and backup file is equal
                        file.state = SmupFile::equal;
                    }
                    else
                    {
                        // source and backup file is unequal
                        counterFilesCopy++;
                        file.state = SmupFile::unequal;
                        file.md5=md5;
                    }
                }
                else
                {
                    if(file.size==fileInfo.size())
                    {
                        // source and backup file is equal
                        file.state = SmupFile::equal;
                    }
                    else
                    {
                        // source and backup file is unequal
                        counterFilesCopy++;
                        file.state = SmupFile::unequal;
                    }
                }
            }
            else
            {
                counterFilesDelete++;
                file.size = fileInfo.size();
                file.state = SmupFile::backup;
            }
            files[dirname+fileInfo.fileName()] = file;
        }
        else if(fileInfo.isDir())
        {
            counterDirectories++;
            scanDir(dirname+fileInfo.fileName());
        }
    }
}

void SmupBackup::run()
{
    scan();

    state = SmupBackup::finish;
    qDebug() << "Finished backup scan thread " << getDir();
}

void SmupBackup::startScan()
{
    qDebug() << "Start backup scan thread " << getDir();
    state = SmupBackup::scanning;

    start();

}
