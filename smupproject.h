#ifndef SMUPPROJECT_H
#define SMUPPROJECT_H

#include <QObject>
#include <QList>
#include <QString>

#include "smupsource.h"
#include "smupbackup.h"

class SmupProject : public QObject
{
    Q_OBJECT
public:
    explicit SmupProject(QObject *parent = 0);
    ~SmupProject();

    bool load(const QString &filename, QString _drive = QString());
    bool save(const QString &filename);

    void clear();
    void reset();

    int size();
    SmupSource* operator[](int pos);
    void addSource(SmupSource *source) { smupSources.append(source); }
    void deleteSource(SmupSource *source) { smupSources.removeOne(source);delete source; }

    void scanSources(bool withMd5 = false);
    void scanBackups(bool withMd5 = false);

    void setAuto(const QString &_automation) { automation = _automation; }
    QString getAuto() { return automation; }

    QString getFilename() { return projectFilename; }

    void setChanged() { changed = true; }
    bool getChanged() { return changed; }

    void setDrive(const QString& _drive) { drive = _drive; }
    QString getDrive() { return drive; }

private:

    QList<SmupSource*> smupSources;

    QString automation;
    QString drive;
    QString projectFilename;

    bool changed;

signals:
    
public slots:
    
};

#endif // SMUPPROJECT_H
