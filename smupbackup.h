#ifndef SMUPBACKUP_H
#define SMUPBACKUP_H

#include <QObject>
#include <QDir>
#include <QThread>
#include <QTreeWidgetItem>

#include "smupfile.h"
#include "smupsource.h"

class SmupBackup : public QThread
{
    Q_OBJECT
public:
    enum State {init,scanning,finish};

    explicit SmupBackup(QObject *parent = 0);
    virtual ~SmupBackup() { disconnect(); }

    void clear();
    void reset();

    void setDir(const QString& _dir) { dir = _dir; }
    QString getDir() { return dir; }

    void setDrive(const QString& _drive) { drive = _drive; }
    QString getDrive() { return drive; }

    void setJob(const QString& _job) { job = _job; }
    QString getJob() { return job; }

    void setItem(QTreeWidgetItem* _item) { item = _item; }
    QTreeWidgetItem* getItem() { return item; }

    State getState() { return state; }

    void setSource(SmupSource *_source) { source = _source; }
    SmupSource* getSource() { return source; }

    QMap<QString,SmupFile> getFilesMap() { return files; }

    unsigned int getFiles() { return counterFiles; }
    quint64 getFilesSize() { return counterSize; }
    unsigned int getFilesNew() { return counterFilesNew; }
    unsigned int getFilesCopy() { return counterFilesCopy; }
    unsigned int getFilesDelete() { return counterFilesDelete; }
    unsigned int getCounterDirectories() { return counterDirectories; }

    void startScan();

    void setWithMD5(bool _withMD5) { withMD5 = _withMD5; }

private:

    void scanDir(QString dirname);

    QString dir;
    QString drive;
    QString job;

    QTreeWidgetItem *item;

    State state;

    unsigned int counterFiles;
    unsigned int counterFilesNew;
    unsigned int counterFilesCopy;
    unsigned int counterFilesDelete;
    unsigned int counterDirectories;
    quint64 counterSize;
    QMap<QString,SmupFile> files;

    SmupSource *source;

    bool withMD5;

    void scan();
    void run();

signals:
    
public slots:
    
};

#endif // SMUPBACKUP_H
