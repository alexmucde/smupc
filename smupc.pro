#-------------------------------------------------
#
# Project created by QtCreator 2012-11-16T19:29:04
#
#-------------------------------------------------

QT       += core gui network
win32 {
    QT += winextras
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = smupc
TEMPLATE = app

icons.path = /usr/share/pixmaps
icons.files = icons/smupc.ico
INSTALLS += icons

desktop.path = /usr/share/applications
desktop.files = smupc.desktop
INSTALLS += desktop

target.path = /usr/bin
INSTALLS += target

SOURCES += main.cpp\
           smupmainwindow.cpp \
    smupsource.cpp \
    smupproject.cpp \
    smupbackup.cpp \
    smupfile.cpp \
    smupform.cpp \
    filesdockwidget.cpp \
    smupjobbroker.cpp \
    qdriveswatch.cpp \
    projectselectiondialog.cpp \
    infodialog.cpp \
    settingsdialog.cpp \
    smupsettings.cpp \
    smupsourcedialog.cpp \
    smupbackupdialog.cpp \
    jobsdockwidget.cpp

HEADERS  += smupmainwindow.h \
    smupsource.h \
    smupproject.h \
    smupbackup.h \
    smupfile.h \
    smupform.h \
    filesdockwidget.h \
    smupjobbroker.h \
    qdriveswatch.h \
    projectselectiondialog.h \
    infodialog.h \
    settingsdialog.h \
    smupsettings.h \
    smupsourcedialog.h \
    smupbackupdialog.h \
    jobsdockwidget.h

FORMS    += smupmainwindow.ui \
    smupform.ui \
    filesdockwidget.ui \
    projectselectiondialog.ui \
    infodialog.ui \
    settingsdialog.ui \
    smupsourcedialog.ui \
    smupbackupdialog.ui \
    jobsdockwidget.ui

OTHER_FILES += \
    example.smu \
    smupc.desktop \
    ReleaseNotes.txt \
    README.txt \
    INSTALL.txt \
    LICENSE

RESOURCES += \
    smup.qrc
