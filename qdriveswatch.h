#ifndef QDRIVESWATCH_H
#define QDRIVESWATCH_H

#include <QObject>
#include <QString>
#include <QStringList>

class QDrivesWatch : public QObject
{
    Q_OBJECT
public:
    explicit QDrivesWatch(QObject *parent = 0);
    
    void setFilename(const QString &_filename);
    QString getFilename();

    void init();
    void watch();

    QStringList getKnownFiles() { return knownFiles; }

private:

    QString filename;
    QStringList knownFiles;

signals:

    void newFile(QString filename,QString drive);
    void removedFile(QString filename);

public slots:
    
};

#endif // QDRIVESWATCH_H
