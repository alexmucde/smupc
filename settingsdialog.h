#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QSettings>

#include "smupproject.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();
    
    void loadSettings(SmupProject &project);
    void saveSettings(SmupProject &project);

private:
    Ui::SettingsDialog *ui;

    QSettings *settings;
};

#endif // SETTINGSDIALOG_H
