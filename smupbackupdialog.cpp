#include <QFileDialog>
#include <QDebug>

#include "smupbackupdialog.h"
#include "ui_smupbackupdialog.h"

SmupBackupDialog::SmupBackupDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SmupBackupDialog)
{
    ui->setupUi(this);
}

SmupBackupDialog::~SmupBackupDialog()
{
    delete ui;
}

void SmupBackupDialog::on_pushButtonDirectory_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Backup Directory"),
                                                 "",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);

    if(!dir.isEmpty())
    {
        ui->lineEditDirectory->setText(dir);
    }
}

void SmupBackupDialog::load(SmupBackup &backup)
{
    int index = ui->comboBoxJob->findText(backup.getJob());
    if ( index != -1 ) { // -1 for not found
       ui->comboBoxJob->setCurrentIndex(index);
    }
    ui->lineEditDirectory->setText(backup.getDir());
}

void SmupBackupDialog::save(SmupBackup &backup)
{
    backup.setJob(ui->comboBoxJob->currentText());
    backup.setDir(ui->lineEditDirectory->text());
}

