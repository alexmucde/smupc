#ifndef SMUPSETTINGS_H
#define SMUPSETTINGS_H

#include <QSettings>

class SmupSettings
{
public:
    SmupSettings();
    ~SmupSettings();

    bool getEnableAutomaticFunctions();
    bool getAutoProjectLoading();
    bool getAutoProjectLoadingStartup();
    bool getAutoProjectClosing();
    bool getUseMD5();

private:
    QSettings *settings;
};

#endif // SMUPSETTINGS_H
