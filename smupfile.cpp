#include <QObject>
#include <QFile>
#include <QCryptographicHash>

#include "smupfile.h"

QByteArray SmupFile::calculateMD5(const QString &filename)
{
    QFile file(filename);
    QCryptographicHash hash(QCryptographicHash::Md5);

    if (!file.open(QIODevice::ReadOnly))
        return QByteArray();

    while (!file.atEnd()) {
        QByteArray data = file.read(1024);
        hash.addData(data);
    }

    file.close();

    return hash.result();
}
