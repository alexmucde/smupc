#include <QTreeWidgetItem>
#include <QDebug>
#include <QMessageBox>

#include "smupform.h"
#include "ui_smupform.h"
#include "smupsourcedialog.h"
#include "smupbackupdialog.h"

SmupForm::SmupForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SmupForm)
{
    ui->setupUi(this);

    QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
    item->setText(0,"Empty");
    ui->treeWidget->insertTopLevelItem(0,item);

    ui->treeWidget->setColumnWidth(0,300); // Directory
    ui->treeWidget->setColumnWidth(1,50);  // Files
    ui->treeWidget->setColumnWidth(2,150); // Size
    ui->treeWidget->setColumnWidth(3,50);  // New
    ui->treeWidget->setColumnWidth(4,50);  // Copy
    ui->treeWidget->setColumnWidth(5,50);  // Delete
}

SmupForm::~SmupForm()
{
    delete ui;
}

void SmupForm::init(SmupProject *_project)
{
    project = _project;

    ui->treeWidget->clear();

    for(int num1=0;num1<project->size();num1++) {
        SmupSource *source = (*project)[num1];
        QTreeWidgetItem *item1 = new QTreeWidgetItem(ui->treeWidget);
        source->setItem(item1);
        if(source->getHost().isEmpty())
            source->getItem()->setText(0,source->getDir());
        else
            source->getItem()->setText(0,source->getDir()+" ("+source->getHost()+")");
        ui->treeWidget->insertTopLevelItem(num1,item1);
        for(int num2=0;num2<source->size();num2++) {
            SmupBackup *backup = (*source)[num2];
            QTreeWidgetItem *item2 = new QTreeWidgetItem(item1);
            backup->setItem(item2);
            item2->setText(0,backup->getDir());
            item1->addChild(item2);
        }
    }

    ui->treeWidget->expandAll();
}

void SmupForm::updateTreeWidget()
{    
    for(int num1=0;num1<project->size();num1++) {
        SmupSource *source = (*project)[num1];
        if(source->getHost().isEmpty())
            source->getItem()->setText(0,source->getDir());
        else
            source->getItem()->setText(0,source->getDir()+" ("+source->getHost()+")");

        if(source->getState() == SmupSource::init)
            source->getItem()->setData(0,Qt::BackgroundRole,QBrush(QColor(255,255,255)));
        else if(source->getState() == SmupSource::scanning)
            source->getItem()->setData(0,Qt::BackgroundRole,QBrush(QColor(255,255,0)));
        else if(source->getState() == SmupSource::finish)
            source->getItem()->setData(0,Qt::BackgroundRole,QBrush(QColor(0,255,0)));

        source->getItem()->setText(1,QString("%1").arg(source->getFiles()));
        quint64 size = source->getFilesSize();
        if(size<1000)
            source->getItem()->setText(2,QString("%1").arg(size));
        else if(size<1000000)
            source->getItem()->setText(2,QString("%1 (%2KB)").arg(size).arg(size/1000ul));
        else if(size<1000000000)
            source->getItem()->setText(2,QString("%1 (%2MB)").arg(size).arg(size/1000000ul));
        else
            source->getItem()->setText(2,QString("%1 (%2GB)").arg(size).arg(size/Q_UINT64_C(1000000000)));
        for(int num2=0;num2<source->size();num2++) {
            SmupBackup *backup = (*source)[num2];
            backup->getItem()->setText(0,backup->getDir());

            if(backup->getState()==SmupBackup::init)
                backup->getItem()->setData(0,Qt::BackgroundRole,QBrush(QColor(255,255,255)));
            else if(backup->getState() == SmupBackup::scanning)
                backup->getItem()->setData(0,Qt::BackgroundRole,QBrush(QColor(255,255,0)));
            else if(backup->getState() == SmupBackup::finish)
                backup->getItem()->setData(0,Qt::BackgroundRole,QBrush(QColor(0,255,0)));

            backup->getItem()->setText(1,QString("%1").arg(backup->getFiles()));
            size = backup->getFilesSize();
            if(size<1000)
                backup->getItem()->setText(2,QString("%1").arg(size));
            else if(size<1000000)
                backup->getItem()->setText(2,QString("%1 (%2KB)").arg(size).arg(size/1000ul));
            else if(size<1000000000)
                backup->getItem()->setText(2,QString("%1 (%2MB)").arg(size).arg(size/1000000ul));
            else
                backup->getItem()->setText(2,QString("%1 (%2GB)").arg(size).arg(size/Q_UINT64_C(1000000000)));
            backup->getItem()->setText(3,QString("%1").arg(backup->getFilesNew()));
            backup->getItem()->setText(4,QString("%1").arg(backup->getFilesCopy()));
            backup->getItem()->setText(5,QString("%1").arg(backup->getFilesDelete()));
        }
    }

}

void SmupForm::update()
{
    updateTreeWidget();
}

void SmupForm::on_treeWidget_itemClicked(QTreeWidgetItem *item, int /*column*/)
{
    filesDockWidget->clear();

    for(int num1=0;num1<project->size();num1++) {
        SmupSource *source = (*project)[num1];
         for(int num2=0;num2<source->size();num2++) {
            SmupBackup *backup = (*source)[num2];
            if(item==backup->getItem()) {
                int type = 1;
                if(backup->getJob()=="sync")
                {
                    type = 1;
                }
                else if(backup->getJob()=="copy")
                {
                    type = 2;
                }
                else if(backup->getJob()=="backup")
                {
                    qDebug() << "Backup not supported yet";
                }
                filesDockWidget->updateTreeFiles(backup->getFilesMap(),type);
                return;
            }
        }
    }
}

void SmupForm::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int /*column*/)
{
    for(int num1=0;num1<project->size();num1++)
    {
        SmupSource *source = (*project)[num1];
        if(item==source->getItem())
        {
            SmupSourceDialog dlg;
            dlg.load(*source);
            if(dlg.exec())
            {
                dlg.save(*source);
                project->setChanged();
            }
            init(project);
            return;
        }
        for(int num2=0;num2<source->size();num2++)
        {
            SmupBackup *backup = (*source)[num2];
            if(item==backup->getItem())
            {
                SmupBackupDialog dlg;
                dlg.load(*backup);
                if(dlg.exec())
                {
                    dlg.save(*backup);
                    project->setChanged();
                }
                init(project);
                return;

            }
        }
    }

    QMessageBox::warning(0,"Edit Item","No source or backup selected!");
}

void SmupForm::addSource()
{
    SmupSource *source = new SmupSource();

    SmupSourceDialog dlg;
    dlg.load(*source);
    if(dlg.exec())
    {
        dlg.save(*source);
        project->addSource(source);
        project->setChanged();

        init(project);
        return;
    }
    else
    {
        delete source;
        return;
    }

}

void SmupForm::addBackup()
{
    QList<QTreeWidgetItem *> items = ui->treeWidget->selectedItems();
    if(items.size() == 1)
    {
        QTreeWidgetItem *item = items[0];

        for(int num1=0;num1<project->size();num1++)
        {
            SmupSource *source = (*project)[num1];
            if(item==source->getItem())
            {
                SmupBackup *backup = new SmupBackup();

                SmupBackupDialog dlg;
                dlg.load(*backup);
                if(dlg.exec())
                {
                    dlg.save(*backup);
                    source->addBackup(backup);
                    project->setChanged();

                    init(project);
                    return;
                }
                else
                {
                    delete source;
                    return;
                }

            }
        }
    }

    QMessageBox::warning(0,"Add Backup","No source selected!");
}

void SmupForm::editItem()
{
    QList<QTreeWidgetItem *> items = ui->treeWidget->selectedItems();
    if(items.size() == 1)
    {
        QTreeWidgetItem *item = items[0];

        on_treeWidget_itemDoubleClicked(item,0);

        return;
    }

    QMessageBox::warning(0,"Edit Item","No source or backup selected!");
}

void SmupForm::deleteItem()
{
    QList<QTreeWidgetItem *> items = ui->treeWidget->selectedItems();
    if(items.size() == 1)
    {
        QTreeWidgetItem *item = items[0];

        for(int num1=0;num1<project->size();num1++)
        {
            SmupSource *source = (*project)[num1];
            if(item==source->getItem())
            {
                project->deleteSource(source);
                project->setChanged();

                init(project);
                return;
            }
            for(int num2=0;num2<source->size();num2++)
            {
                SmupBackup *backup = (*source)[num2];
                if(item==backup->getItem())
                {
                    source->deleteBackup(backup);
                    project->setChanged();

                    init(project);
                    return;
                }
            }
        }
    }

    QMessageBox::warning(0,"Delete Item","No source or backup selected!");
}
