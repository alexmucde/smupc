SMUP Client  - Readme
=====================
Alexander Wenzel

Overview
--------
The SMUP Client is currently a simple backup or syncing tool.

Documentation
-------------
- SMUP Client Release Notes: ReleaseNotes.txt

Software/Hardware
-----------------

* Developped with: QT 5.6.2 (http://qt-project.org/)
* Ubuntu Linux 12.10 Qt SDK v1.2.1 32-bit / Intel PC
* MS Windows 10 / Intel PC

Dynamically linked open source software
---------------------------------------

* Qt 5.6.2 (LGPL)
* GCC (GPL)
* (Optional) Mingw (GPL + BSD Variante + Public Domain)

Homepage
--------
http://bitbucket.org/alexmucde/smupc

Contact
-------
http://bitbucket.org/alexmucde/smupc
